FROM ruby:2.6.1

# adduser creates folder under /home with the right permissions
# mv is a rename and not a copy of the folder name when the destination folder does not exist
ENV HOME=/home/arachni/downloads
RUN addgroup -system arachni && \
    adduser -system arachni && \
    usermod -g arachni arachni

# Name of the arachni main folder contained in the .tar.gz
ARG ARACHNI=1.5.1
ARG WEBUI=0.5.12
ENV URL=https://github.com/Arachni/arachni/releases/download/v${ARACHNI}/arachni-${ARACHNI}-${WEBUI}-linux-x86_64.tar.gz

USER arachni
WORKDIR /home/arachni
RUN wget ${URL} --output-document arachni.tar.gz
RUN tar zxvf arachni.tar.gz && \
    rm arachni.tar.gz && \
    mv -v arachni-${ARACHNI}-${WEBUI} $HOME

ARG COMMIT
LABEL org.opencontainers.image.revision=$COMMIT

ENV PATH="/home/arachni/downloads/bin:${PATH}"
RUN mkdir -p build && \
    echo $COMMIT > $HOME/commit
WORKDIR /home/arachni/build

