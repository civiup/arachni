#!/bin/bash

# avoids running twice https://gitlab.com/gitlab-org/gitlab-runner/issues/1380#note_120576018
if [ -f ran.txt ]; then
  exit 0
fi
touch ran.txt

echo "Container built at https://gitlab.com/civiup/arachni commit `cat commit`"

./bin/arachni --output-verbose --scope-include-subdomains https://webapp-7egl.onrender.com --report-save-path=webapp-7egl.onrender.com

./bin/arachni_reporter webapp-7egl.onrender.com --reporter=html:outfile=report.html.zip

rm -rf public

unzip report.html.zip -d public

bash -c "$*"
